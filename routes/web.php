<?php

use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;
use App\Http\Controllers\Admin\LoginController;
use App\Http\Controllers\Admin\ListingController;
use App\Http\Controllers\Admin\CategoriesCrudController;
use App\Http\Controllers\Admin\ProductCrudController;
use App\Http\Controllers\Admin\SearchController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return Inertia::render('Welcome', [
        'canLogin' => Route::has('login'),
        'canRegister' => Route::has('register'),
        'laravelVersion' => Application::VERSION,
        'phpVersion' => PHP_VERSION,
    ]);
});

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return Inertia::render('Dashboard');
})->name('dashboard');

Route::GET('/admin/login', [LoginController::class, 'login'])->name('admin.login');

Route::POST('admin/login', [LoginController::class, 'loginPost'])->name('admin.loginPost');

Route::GET('/admin/logout', [LoginController::class, 'logout'])->name('admin.logout');


Route::prefix('admin')->middleware(['admin'])->group(function () {

    Route::GET('/', [LoginController::class, 'dashboard'])->name('admin.dashboard');

    Route::GET('/listing/{model}', [ListingController::class, 'index'])->name('listing.index');

    Route::GET('/search', [SearchController::class, 'search'])->name('search');

    Route::GET('/category/detail/{id}/{name}', [CategoriesCrudController::class, 'categoryClass'])->name('category.categoryClass');

    Route::GET('/category/delete/{id}/{name}', [CategoriesCrudController::class, 'categoryDelete'])->name('category.categoryDelete');

    Route::GET('/product/delete/{id}/{name}', [ProductCrudController::class, 'productDelete'])->name('product.productDelete');


    Route::resource('product', ProductCrudController::class)->except('show', 'destroy');

    Route::resource('category', CategoriesCrudController::class)->except('show', 'destroy');
});
