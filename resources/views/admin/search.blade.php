@extends('layouts.admin')

@section('contentOne')
    <script>
        var msg = '{{Session::get('message')}}';
        var exist = '{{Session::has('message')}}';
        if(exist){
            alert(msg);
        }
    </script>
    <div class="">

        <div class="clearfix"></div>

        <div class="row" style="display: block;">
            <div class="clearfix"></div>
            <div class="col-md-12 col-sm-12  ">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Đã tìm thấy {{$totol}} sản phẩm</h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>

                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">

                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Tên sản phẩm</th>
                                <th>Giá sản phẩm</th>
                                <th>Số lượng</th>
                                <th>Đã bán</th>
                                <th>Thông tin</th>
                                <th>Danh mục</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if($products)
                                @foreach($products as $product)
                                    <tr>
                                        <th scope="row">{{$product->name}}</th>
                                        <td>{{$product->price}}</td>
                                        <td>{{$product->quantity}}</td>
                                        <td>{{$product->selled}}</td>
                                        <td>{{$product->description}}</td>
                                        <td>
                                            @foreach($product->categories as $item)
                                                {{$item->name}},
                                            @endforeach
                                        </td>
                                        <td style="width: 200px">
                                            <a href="/admin/product/edit/{{$product->id}}/{{\Str::slug($product->name)}}" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> Edit </a>
                                            <a href="/admin/product/delete/{{$product->id}}/{{\Str::slug($product->name)}}" onclick="return confirm('Bạn chắc chắn xóa sản phẩm này?')" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Delete </a>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
@endsection
