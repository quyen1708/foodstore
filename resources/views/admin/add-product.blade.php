@extends('layouts.admin')

@section('contentOne')
    <script>
        var msg = '{{Session::get('messageAdd')}}';
        var exist = '{{Session::has('messageAdd')}}';
        if(exist){
            alert(msg);
        }
    </script>
    <div class="">
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="x_panel">
                    <div class="x_title">
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix">
                            <span class="section">Product info</span>
                        </div>
                    </div>
                    <div class="x_content">
                        @if($errors->any())
                            @foreach($errors->all() as $error)
                                <div class="alert alert-danger" role="alert">
                                    {{$error}}
                                </div>
                            @endforeach
                        @endif
                        <form action="{{route('product.store')}}" method="post"  novalidate>
                            @csrf
                            <div class="field item form-group">
                                <label class="col-form-label col-md-3 col-sm-3  label-align">Tên sản phẩm<span class="required">*</span></label>
                                <div class="col-md-6 col-sm-6">
                                    <input class="form-control" type="text" name="name" placeholder="ex. Sữa tươi không đường TH true MILK" required="required" />
                                </div>
                            </div>
                            <div class="field item form-group">
                                <label class="col-form-label col-md-3 col-sm-3  label-align">Giá sản phẩm<span class="required">*</span></label>
                                <div class="col-md-6 col-sm-6">
                                    <input class="form-control" type="number" pattern="([0-9]{1,3}).([0-9]{1,3})" name="price" required="required" />
                                </div>
                            </div>
                            <div class="field item form-group">
                                <label class="col-form-label col-md-3 col-sm-3  label-align">Số lượng<span class="required">*</span></label>
                                <div class="col-md-6 col-sm-6">
                                    <input class="form-control" type="number" name="quantity" required="required" />
                                </div>
                            </div>
                            <div class="field item form-group">
                                <label class="col-form-label col-md-3 col-sm-3  label-align">Mô tả sản phẩm<span class="required">*</span></label>
                                <div class="col-md-6 col-sm-6">
                                    <textarea class="form-control" type="textarea" name="description" id="description" maxlength="6000" rows="7" placeholder="Description...."></textarea>
                                </div>
                            </div>
                            <div class="field item form-group">
                                <label class="col-form-label col-md-3 col-sm-3  label-align">Danh mục<span class="required">*</span></label>
                                <div class="col-md-6 col-sm-6">
                                    <select  class="form-control categories" name="categories[]" multiple="multiple">
                                        @if($cates)5
                                            @foreach($cates as $cate)
                                        <option value="{{$cate->id}}" >{{$cate->name}}</option>
                                            @endforeach
                                        @endif
                                    </select>
{{--                                   @if($cates)--}}
{{--                                       @foreach($cates as $cate)--}}
{{--                                            <input type="checkbox" name="show_option[]" data-id="{{$cate->id}}" value="{{$cate->id}}">--}}
{{--                                            <label for="{{$cate->name}}"> {{$cate->name}}</label><br>--}}
{{--                                        @endforeach--}}
{{--                                    @endif--}}
                                </div>
                            </div>
                            <div class="ln_solid">
                                <div class="form-group">
                                    <div class="col-md-6 offset-md-3">
                                        <button type='submit' class="btn btn-primary">Submit</button>
                                        <button type='reset' class="btn btn-success">Reset</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
