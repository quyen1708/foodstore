@extends('layouts.admin')

@section('contentOne')
    <script>
        var msg = '{{Session::get('messageAdd')}}';
        var exist = '{{Session::has('messageAdd')}}';
        if(exist){
            alert(msg);
        }
    </script>
    <div class="">
        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="x_panel">
                    <div class="x_title">
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        @if($errors->any())
                            @foreach($errors->all() as $error)
                                <div class="alert alert-danger" role="alert">
                                    {{$error}}
                                </div>
                            @endforeach
                        @endif
                        <form action="{{route('product.update', $product->id)}}" method="POST"  novalidate>
                            @method('PATCH')
                            @csrf
                            <span class="section">Product info</span>
                            <div class="field item form-group">
                                <label class="col-form-label col-md-3 col-sm-3  label-align">Tên sản phẩm<span class="required">*</span></label>
                                <div class="col-md-6 col-sm-6">
                                    <input class="form-control" type="text" name="name" placeholder="ex. Sữa tươi không đường TH true MILK" required="required" value="{{$product->name}}"/>
                                </div>
                            </div>
                            <div class="field item form-group">
                                <label class="col-form-label col-md-3 col-sm-3  label-align">Giá sản phẩm<span class="required">*</span></label>
                                <div class="col-md-6 col-sm-6">
                                    <input class="form-control" type="number" pattern="([0-9]{1,3}).([0-9]{1,3})" name="price" required="required" value="{{$product->price}}"/>
                                </div>
                            </div>
                            <div class="field item form-group">
                                <label class="col-form-label col-md-3 col-sm-3  label-align">Số lượng<span class="required">*</span></label>
                                <div class="col-md-6 col-sm-6">
                                    <input class="form-control" type="number" name="quantity" required="required" value="{{$product->quantity}}" />
                                </div>
                            </div>
                            <div class="field item form-group">
                                <label class="col-form-label col-md-3 col-sm-3  label-align">Mô tả sản phẩm<span class="required">*</span></label>
                                <div class="col-md-6 col-sm-6">
                                    <textarea class="form-control" type="textarea" name="description" id="description" maxlength="6000" rows="7" placeholder="Description....">{{$product->description}}</textarea>
                                </div>
                            </div>
                            <div class="field item form-group">
                                <label class="col-form-label col-md-3 col-sm-3  label-align">Danh mục<span class="required">*</span></label>
                                <div class="col-md-6 col-sm-6">
                                    <select  class="form-control categories" name="categories[]" multiple="multiple">
                                        <?php
                                        foreach($cates as $cate){
                                            $flag = 0;
                                            foreach($product->categoriesActive as $item){
                                                if($cate->id == $item->id){
                                                     $flag = 1;
                                                }
                                            }
                                        if ($flag == 0){
                                            echo "<option value=$cate->id> $cate->name </option>";
                                            }
                                            if ($flag == 1){
                                                echo "<option value=$cate->id selected> $cate->name </option>";
                                                $flag = 0;
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="ln_solid">
                                <div class="form-group">
                                    <div class="col-md-6 offset-md-3">
                                        <button type='submit' class="btn btn-primary">Submit</button>
                                        <button type='reset' class="btn btn-success">Reset</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
