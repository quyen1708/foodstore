<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('admins')->insert([
            'name' => 'Quyen',
            'email' => 'vhkb1708@gmail.com',
            'password' => Hash::make('12345678'),
        ]);

        for ($i = 0; $i < 15; $i++) {
            DB::table('categories')->insert([
                'name' => Str::random(20),
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ]);
        }

        for ($i = 0; $i < 100; $i++) {
            DB::table('products')->insert([
                'name' => Str::random(10),
                'description' => Str::random(20),
                'price' => random_int(5000, 200000),
                'quantity' => random_int(100, 200),
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ]);
        }
        for ($i = 0; $i < 100; $i++) {
            DB::table('products_categories')->insert([
                'category_id' => random_int(1, 15),
                'product_id' => random_int(1, 100),
            ]);
        }
    }

}

