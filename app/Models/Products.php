<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;

class Products extends Model
{
    use HasFactory;
    protected $table = 'products';
    protected $guarded = ['id'];

    public function categoriesActive(){
        return $this->belongsToMany(Categories::class, 'products_categories', 'product_id','category_id')
            ->where('status',1);
    }

    public function categoriesDestroyed(){
        return $this->belongsToMany(Categories::class, 'products_categories', 'product_id','category_id')
            ->where('status',0);
    }
}
