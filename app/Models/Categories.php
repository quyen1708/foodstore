<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;

class Categories extends Model
{
    use HasFactory;
    protected $table = 'categories';
    protected $guarded = ['id'];

    public function productsActive(){
        return $this->belongsToMany(Products::class, 'products_categories', 'category_id', 'product_id')
            ->where('status',1);
    }

    public function productsDestroyed(){
        return $this->belongsToMany(Products::class, 'products_categories', 'category_id', 'product_id')
            ->where('status',0);
    }
}
