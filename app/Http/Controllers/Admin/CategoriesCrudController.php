<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Categories;
use App\Models\Products;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\CategoryCreateRequest;

class CategoriesCrudController extends Controller
{
    public function create()
    {
        $adminUser = Auth::guard('admin')->user();
        $cates = Categories::where('status', 1)
            ->select('name', 'id')
            ->get();
        return view('admin.addcategory',[
            'user'  =>  $adminUser,
            'cates' =>  $cates
        ]);
    }

    public function store(CategoryCreateRequest $request)
    {
        $category = Categories::create($request->only('name'));
        return redirect()->route('category.create')->with('messageAdd', 'Thêm thành công sản phẩm mới');
    }

    public function categoryClass(Request $request)
    {
        $adminUser = Auth::guard('admin')->user();
        $cates = Categories::where('status', 1)
            ->select('name', 'id')
            ->get();
        $products = Categories::find($request->id)->productsActive()->get();
        return view('admin.categoryClass',[
            'user'      =>  $adminUser,
            'cates'     =>  $cates,
            'products'  =>  $products
        ]);
    }

    public function index()
    {
        $adminUser = Auth::guard('admin')->user();
        $cates = Categories::where('status', 1)
            ->select('name', 'id')
            ->get();
        return view('admin.category',[
            'user'      =>  $adminUser,
            'cates'     =>  $cates
        ]);
    }

    public function edit($id)
    {
        $adminUser = Auth::guard('admin')->user();
        $cates = Categories::where('status', 1)
            ->select('name', 'id')
            ->get();
        $category = Categories::findOrFail($id);
//        dd($product->categories);
        return view('admin.edit-category',[
            'user'      =>  $adminUser,
            'cates'     =>  $cates,
            'category'   =>  $category
        ]);
    }

    public function update(CategoryCreateRequest $request, $id){
        $category = Categories::find($id);
        $category->name = $request->name;
        $category->save();
        return redirect()->route('category.index')->with('message', 'Thay đổi danh mục thành công');
    }

    public function categoryDelete($id){
        $product = Categories::find($id);
        $product->status = 0;
        $product -> save();
        return redirect()->route('category.index')->with('message', 'Xóa sản phẩm thành công');
    }
}

