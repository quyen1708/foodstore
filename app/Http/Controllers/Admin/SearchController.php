<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Categories;
use App\Models\Products;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SearchController extends Controller
{
    public function search(Request $request)
    {

        $adminUser = Auth::guard('admin')->user();
        $cates = Categories::where('status', 1)
            ->select('name', 'id')
            ->get();

        $q = $request->get('search', '');

        $total = Products::with('categories')
            ->where('status', 1)
            ->where('quantity','>',0)
            ->where('name', 'LIKE', '%' . $q . '%')
            ->orWhere('description', 'LIKE', '%' . $q . '%')
            ->count();
        $products = Products::with('categories')
            ->where('status', 1)
            ->where('quantity','>',0)
            ->where('name', 'LIKE', '%' . $request->search . '%')
            ->orWhere('description', 'LIKE', '%' . $q . '%')
            ->paginate(6);

//        dd($products);

        return view('admin.search',[
        'totol'     =>  $total,
        'products' =>  $products,
        'user'      =>  $adminUser,
        'cates'     =>  $cates
    ]);
    }
}
