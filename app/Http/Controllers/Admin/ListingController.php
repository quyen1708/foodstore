<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Categories;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ListingController extends Controller
{
    public function index(Request $request, $model)
    {
        $adminUser = Auth::guard('admin')->user();
        $cates = Categories::query()
            ->where('status', 1)
            ->select('name', 'id')
            ->get();
        return view('admin.listing',[
            'user'=>$adminUser,
            'cates'=>$cates
            ]);
    }
}
