<?php

namespace App\Http\Controllers\Admin;

use App\Models\Categories;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    public function login()
    {
        return view('admin.login');
    }
    public function loginPost(Request $request)
    {
        $credentials = $request->only('email', 'password');
        if (Auth::guard('admin')->attempt($credentials)) {
            return redirect()->route('admin.dashboard');
        } else {
            return ("dang nhap khong thanh cong");
        }
    }
    public function dashboard()
    {
        $cates = Categories::query()
            ->where('status', 1)
            ->select('name', 'id')
            ->get();
        $adminUser = Auth::guard('admin')->user();
        return view('admin.dashboard',[
            'user'  =>  $adminUser,
            'cates' =>  $cates
            ]);
    }

    public function logout()
    {
       Auth::guard('admin')->logout();
       return redirect()->route('admin.login');
    }
}
