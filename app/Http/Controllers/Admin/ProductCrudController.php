<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Categories;
use App\Models\Products;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\ProductCreateRequest;

class ProductCrudController extends Controller
{

    public function create()
    {
        $adminUser = Auth::guard('admin')->user();
        $cates = Categories::where('status', 1)
            ->select('name', 'id')
            ->get();
        return view('admin.add-product',[
            'user'  =>  $adminUser,
            'cates' =>  $cates
        ]);
    }

    public function store(ProductCreateRequest $request)
    {
//        dd($request->name);
//        dd($request->input('categories'));

//        $arrayToString = implode(',', $request->input('show_option'));
//        dd($arrayToString);
        $product = Products::create($request->only('name','price','quantity','description'));
        $categories = Categories::find($request->input('categories'));
        $product->categoriesActive()->attach($categories);
        return redirect()->route('product.create')->with('messageAdd', 'Thêm thành công sản phẩm mới');
    }

    public function index()
    {
        $adminUser = Auth::guard('admin')->user();
        $cates = Categories::where('status', 1)
            ->select('name', 'id')
            ->get();
        $products = Products::where('status', 1)
            ->with('categoriesActive')
            ->get();
        return view('admin.product',[
            'user'      =>  $adminUser,
            'cates'     =>  $cates,
            'products'   =>  $products
        ]);
    }

    public function edit($id)
    {
        $adminUser = Auth::guard('admin')->user();
        $cates = Categories::select('name', 'id')
            ->where('status', 1)
            ->get();
        $product = Products::with('categoriesActive')->findOrFail($id);
//        dd($product->categories);
        return view('admin.edit-product',[
            'user'      =>  $adminUser,
            'cates'     =>  $cates,
            'product'   =>  $product
        ]);
    }

    public function update(ProductCreateRequest $request, $id){
        $product = Products::find($id);
        $product->name = $request->name;
        $product->price = $request->price;
        $product->quantity = $request->quantity;
        $product->description = $request->description;
        $product->save();
        $categories = Categories::find($request->input('categories'));
        $product->categoriesActive()->sync($categories);
        return redirect()->route('product.index')->with('message', 'Thay đổi sản phẩm thành công');
    }

    public function productDelete($id){
        $product = Products::find($id);
        $product->status = 0;
        $product -> save();
        return redirect()->route('product.index')->with('message', 'Xóa sản phẩm thành công');
    }
}
