<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'required|max:255',
            'description'=>'required|max:6000',
            'price'=>'required|numeric|min:4',
            'quantity'=>'required|numeric'
        ];
    }
    public function messages()
    {
        return [
            'name.required'=>'Name is required!',
            'description.required'=>'Description is required!',
            'name.max'=>'Name is no longger than 255 character!',
            'description.max'=>'Description too big!',
            'price.required' =>'Price is required!',
            'price.min' => 'Price must have bigger 4 digits',
            'quantity.required' => 'Quantity is required!',
            'quantity.numeric'=>'Quantity must be number'
        ];
    }
}
